package de.starmixcraft.libmatica.gui;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;

public class RenderUtils {
	
	
	public static final int WHITE = 0xffffffff;
	public static final int BLACK = 0xff000000;
	public static final int RED = 0xffff0000;
	public static final int GREEN = 0xff00ff00;
	public static final int BLUE = 0xff0000ff;

	public static void renderString(FontRenderer renderer, int x, int y, String text, int color, float scail) {
		GL11.glPushMatrix(); // Start new matrix
		GL11.glScalef(scail, scail, scail); // scale it to 0.5 size on each side. Must be float e.g.: 2.0F
		renderer.drawStringWithShadow(text, x / scail, y / scail, color); // fr - fontRenderer
		GL11.glPopMatrix(); // End this matrix
	}

	public static void renderStringCentered(FontRenderer renderer, int x, int y, String text, int color, float scail) {
		int width = (int) (renderer.getStringWidth(text) * scail);
		renderString(renderer, x - width / 2, y, text, color, scail);
	}

	public static void fillRec(int x, int y, int width, int height, int color) {
		fill(x, y, x+width, y+height, color, color);
	}
	
	public static void fill(int x1, int y1, int x2,
			int y2, int startGradient, int endGradiant) {
		float f = (float) (startGradient >> 24 & 255) / 255.0F;
		float f1 = (float) (startGradient >> 16 & 255) / 255.0F;
		float f2 = (float) (startGradient >> 8 & 255) / 255.0F;
		float f3 = (float) (startGradient & 255) / 255.0F;
		float f4 = (float) (endGradiant >> 24 & 255) / 255.0F;
		float f5 = (float) (endGradiant >> 16 & 255) / 255.0F;
		float f6 = (float) (endGradiant >> 8 & 255) / 255.0F;
		float f7 = (float) (endGradiant & 255) / 255.0F;
		GlStateManager.disableTexture();
		GlStateManager.enableBlend();
		GlStateManager.disableAlphaTest();
		GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
				GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		GlStateManager.shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.pos((double) x2, (double) y1, (double) 0).color(f1, f2, f3, f)
				.endVertex();
		bufferbuilder.pos((double) x1, (double) y1, (double) 0).color(f1, f2, f3, f)
				.endVertex();
		bufferbuilder.pos((double) x1, (double) y2, (double) 0).color(f5, f6, f7, f4)
				.endVertex();
		bufferbuilder.pos((double) x2, (double) y2, (double) 0).color(f5, f6, f7, f4)
				.endVertex();
		tessellator.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableTexture();
		GlStateManager.enableBlend();
	}
}
