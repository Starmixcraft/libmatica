package de.starmixcraft.libmatica.gui.list;

import java.awt.Color;
import java.io.File;

import com.mojang.blaze3d.platform.GlStateManager;
import com.sun.jna.platform.win32.WinDef.HICON;

import de.starmixcraft.libmatica.gui.LoadSchematicGui;
import de.starmixcraft.libmatica.gui.RenderUtils;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.fml.client.config.HoverChecker;

public class SchemListItem extends ExtendedList.AbstractListEntry<SchemListItem> {

	private File f;
	private ItemStack renderItem;

	public SchemListItem(File f) {
		this.f = f;
		// select random item based on filename
		short b = 0;
		for (char s : f.getName().toCharArray()) {
			b += s;
			if (b < 0) {
				b = 0;
			}
			if (b > Registry.ITEM.keySet().size()) {
				b = 0;
			}
		}
		renderItem = Item.getItemById(b).getDefaultInstance();
	}

	@Override
	public void render(int index, int y, int x, int width, int height, int mouseX, int mouseY, boolean hovered,
			float partialtick) {
		if (hovered) {
			RenderUtils.fillRec(x-1, y-1, width-1, height+2, RenderUtils.GREEN);
			RenderUtils.fillRec(x, y, width-3, height, RenderUtils.BLACK);
		}


		Minecraft.getInstance().fontRenderer.drawString(f.getName(), x, y + 20, RenderUtils.WHITE);
		RenderHelper.enableGUIStandardItemLighting();
		Minecraft.getInstance().getItemRenderer().renderItemIntoGUI(renderItem, x, y);
		RenderHelper.disableStandardItemLighting();

	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		if(list.getSelected() == this) {
			LoadSchematicGui.loadFile(f.getName());
			return true;
		}
		list.setSelected(this);
		return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
	}

}
