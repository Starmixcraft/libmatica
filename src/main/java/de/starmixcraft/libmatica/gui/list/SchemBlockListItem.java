package de.starmixcraft.libmatica.gui.list;

import java.awt.Color;
import java.io.File;

import com.mojang.blaze3d.platform.GlStateManager;
import com.sun.jna.platform.win32.WinDef.HICON;

import de.starmixcraft.libmatica.gui.LoadSchematicGui;
import de.starmixcraft.libmatica.gui.RenderUtils;
import de.starmixcraft.libmatica.schematic.SchematicMaterialList.BlockCount;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.fml.client.config.HoverChecker;

public class SchemBlockListItem extends ExtendedList.AbstractListEntry<SchemBlockListItem> {

	private ItemStack renderItem;
	private Block b;
	private BlockCount blockCount;
	
	public SchemBlockListItem(Block b, BlockCount c) {
		this.blockCount = c;
		renderItem = getRenderStack(b);
		this.b = b;
	}

	@Override
	public void render(int index, int y, int x, int width, int height, int mouseX, int mouseY, boolean hovered,
			float partialtick) {
		RenderHelper.enableGUIStandardItemLighting();
		Minecraft.getInstance().getItemRenderer().renderItemIntoGUI(renderItem, x, y+5);
		RenderHelper.disableStandardItemLighting();
		String color = "�c";
		if(blockCount.getMissing() == 0) {
			color = "�a";
		}
		String s = color + b.getNameTextComponent().getString() + " " + blockCount.getGot() + "/" + blockCount.getNeeded() + " missing: " + blockCount.getMissing();
		Minecraft.getInstance().fontRenderer.drawString(s, x+20, y+10, Color.WHITE.getRGB());

	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {

		return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
	}
	
	
	private static ItemStack getRenderStack(Block b) {
		if(b == Blocks.LAVA) {
			return Items.LAVA_BUCKET.getDefaultInstance();
		}
		if(b == Blocks.WATER || b == Blocks.BUBBLE_COLUMN) {
			return Items.WATER_BUCKET.getDefaultInstance();
		}
		if(b == Blocks.TALL_SEAGRASS) {
			return Items.SEAGRASS.getDefaultInstance();
		}
		if(b == Blocks.KELP_PLANT) {
			return Items.KELP.getDefaultInstance();
		}
		return b.asItem().getDefaultInstance();
	}

}
