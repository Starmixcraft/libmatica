package de.starmixcraft.libmatica.gui;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import de.starmixcraft.libmatica.gui.wadgets.BlockPosSelectionButton;
import de.starmixcraft.libmatica.gui.wadgets.IntSelectionButton;
import de.starmixcraft.libmatica.gui.wadgets.LibmaticaButton;
import de.starmixcraft.libmatica.renderer.filter.LayeredFilter;
import de.starmixcraft.libmatica.renderer.filter.LayersAboveFilter;
import de.starmixcraft.libmatica.renderer.filter.LayersBelowFilter;
import de.starmixcraft.libmatica.renderer.filter.SchematicFilter;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.gui.widget.button.CheckboxButton;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.gen.layer.Layer;

public class LibmaticaMainGui extends Screen {

	public LibmaticaMainGui() {
		super(new StringTextComponent("Main GUI"));

	}

	private BlockPosSelectionButton pos;

	public static boolean renderTileEntitys = true;
	public static boolean renderSchem = true;
	public static boolean renderDebugBoxes = false;
	
	private static int currentlayer;

	@Override
	protected void init() {
		Minecraft.getInstance().keyboardListener.enableRepeatEvents(true);
		boolean isSchemLoaded = RenderabelSchematic.INSTANCE != null;
		if (isSchemLoaded) {
			addButton(new LibmaticaButton(width - 105, height - 25, 100, "Unload Schematic", (b) -> {
				RenderabelSchematic.INSTANCE.unload();
				Minecraft.getInstance().displayGuiScreen(LibmaticaMainGui.this);
			}));
			pos = new BlockPosSelectionButton(font, width / 2 - 60, height / 2 - 20, 120, 40, "Position");
			pos.setBlockPos(RenderabelSchematic.INSTANCE.getSchemPos());
			pos.setUpdateListener((pos) -> {
				RenderabelSchematic.INSTANCE.move(pos);
			});
			addButton(pos);

			CheckboxButton toggleWidget3 = new CheckboxButton(10, height - 75, 120, 20, "Render Debug boxes",
					renderDebugBoxes) {
				@Override
				public void onPress() {
					super.onPress();
					renderDebugBoxes = this.func_212942_a();
				}
			};
			addButton(toggleWidget3);

			CheckboxButton toggleWidget = new CheckboxButton(10, height - 50, 120, 20, "Render Tile Entitys",
					renderTileEntitys) {
				@Override
				public void onPress() {
					super.onPress();
					renderTileEntitys = this.func_212942_a();
				}
			};
			addButton(toggleWidget);

			CheckboxButton toggleWidget2 = new CheckboxButton(10, height - 25, 120, 20, "Render schem", renderSchem) {
				@Override
				public void onPress() {
					super.onPress();
					renderSchem = this.func_212942_a();
				}
			};
			addButton(toggleWidget2);

			
			
			IntSelectionButton button = new IntSelectionButton(font, width - 210, height - 55, 100, "Layer");
			button.setValue(currentlayer);
			button.setValueListener((i) -> {
				if (i < 0) {
					i = 0;
					button.setValue(i);
				} else if (i >= RenderabelSchematic.INSTANCE.getHeight()) {
					i = RenderabelSchematic.INSTANCE.getHeight() - 1;
					button.setValue(i);
				}
				currentlayer = i;
				SchematicFilter filter = RenderabelSchematic.INSTANCE.getFilter();
				if (filter instanceof LayeredFilter) {
					((LayeredFilter) filter).setLayer(i);
					RenderabelSchematic.INSTANCE.flagAllChunksInSchemDirty(false);
				}
			});
			addButton(button);

			int index1 = RenderabelSchematic.INSTANCE.getFilter() == null ? 0
					: (RenderabelSchematic.INSTANCE.getFilter().getClass().equals(LayeredFilter.class) ? 1
							: (RenderabelSchematic.INSTANCE.getFilter().getClass().equals(LayersBelowFilter.class)
									? 2
									: (RenderabelSchematic.INSTANCE.getFilter().getClass()
											.equals(LayersAboveFilter.class) ? 3 : 0)));
			
			addButton(new LibmaticaButton(width - 105, height - 55, 100, index1 == 0 ? "Show All" : (index1 == 1 ? "Layer" : (index1 == 2 ? "Layers Below" : "Layers Above")), new IPressable() {
				int index = index1;

				@Override
				public void onPress(Button b) {
					if (index >= 3) {
						index = -1;
					}
					RenderabelSchematic renderabelSchematic = RenderabelSchematic.INSTANCE;
					switch (++index) {
					case 0:
						renderabelSchematic.setFilter(null);
						button.active = false;
						b.setMessage("Show All");
						break;
					case 1:
						renderabelSchematic.setFilter(new LayeredFilter(currentlayer));
						b.setMessage("Layer");
						button.active = true;
						break;
					case 2:
						renderabelSchematic.setFilter(new LayersBelowFilter(currentlayer));
						b.setMessage("Layers Below");
						button.active = true;
						break;
					case 3:
						renderabelSchematic.setFilter(new LayersAboveFilter(currentlayer));
						b.setMessage("Layers Above");
						button.active = true;
						break;
					}
	
				}
			}));
			
			addButton(new LibmaticaButton(width - 105, height - 80, 100, "Materials", (b)->{
				Minecraft.getInstance().displayGuiScreen(new MaterialListGui(RenderabelSchematic.INSTANCE.takeSnapshot()));
			}));
		} else {
			addButton(new LibmaticaButton(width / 2 - 201, height / 2 + 20, 200, "Load Schematic", (b) -> {
				Minecraft.getInstance().displayGuiScreen(new LoadSchematicGui.GuiScreen());
			}));
			addButton(new LibmaticaButton(width / 2 + 1, height / 2 + 20, 200, "Save Schematic", (b) -> {
				Minecraft.getInstance().displayGuiScreen(new SaveSchematicGui());
			}));
		}
		super.init();
	}

	@Override
	public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
		// TODO Auto-generated method stub
		return super.charTyped(p_charTyped_1_, p_charTyped_2_);
	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		// TODO Auto-generated method stub
		return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
	}

	@Override
	public boolean mouseReleased(double p_mouseReleased_1_, double p_mouseReleased_3_, int p_mouseReleased_5_) {
		// TODO Auto-generated method stub
		return super.mouseReleased(p_mouseReleased_1_, p_mouseReleased_3_, p_mouseReleased_5_);
	}

	@Override
	public void mouseMoved(double p_212927_1_, double p_212927_3_) {
		// TODO Auto-generated method stub
		super.mouseMoved(p_212927_1_, p_212927_3_);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		renderBackground();
		boolean isSchemLoaded = RenderabelSchematic.INSTANCE != null;
		if (isSchemLoaded) {
			drawString(font, "Loaded Schematic: " + "{name}", 2, 2, Color.WHITE.getRGB());
		} else {
			RenderUtils.renderStringCentered(font, width / 2, height / 2 - 50, "Libmatica", Color.orange.getRGB(), 4f);
		}
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

}
