package de.starmixcraft.libmatica.gui;

import java.awt.Color;
import java.util.Iterator;

import com.mojang.blaze3d.platform.GlStateManager;

import de.starmixcraft.libmatica.gui.list.SchemBlockListItem;
import de.starmixcraft.libmatica.gui.list.SchemListItem;
import de.starmixcraft.libmatica.schematic.SchematicMaterialList;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.StringTextComponent;

public class MaterialListGui extends Screen {
	
	private SchematicMaterialList materialList;
	protected MaterialListGui(SchematicMaterialList materialList) {
		super(new StringTextComponent("Materials"));
		this.materialList = materialList;
	}	

	private Matlist matlist;
	protected void init() {
		matlist = new Matlist(getMinecraft(), width, height, 20, height-20, 22);
		for (Iterator<Block> blocks = materialList.getBlockItterator();blocks.hasNext();) {
			Block b = blocks.next();
			matlist.addEntry(new SchemBlockListItem(b, materialList.getCount(b)));
		}
		children.add(matlist);

	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
			matlist.render(p_render_1_, p_render_2_, p_render_3_);
	}
	
	
	public static class Matlist extends ExtendedList<SchemBlockListItem>{

		public Matlist(Minecraft mcIn, int widthIn, int heightIn, int topIn, int bottomIn, int slotHeightIn) {
			super(mcIn, widthIn, heightIn, topIn, bottomIn, slotHeightIn);
		}
		
		@Override
		public int addEntry(SchemBlockListItem p_addEntry_1_) {
			// TODO Auto-generated method stub
			return super.addEntry(p_addEntry_1_);
		}
		
	}
}
