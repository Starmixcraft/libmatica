package de.starmixcraft.libmatica.gui.wadgets;

import java.util.function.Consumer;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;

public class IntSelectionButton extends Widget {

	private int value;

	private TextFieldWidget textFieldWidget;
	private Button upButton;
	private Button downButton;
	private FontRenderer renderer;
	private boolean textAbove;
	
	
	private Consumer<Integer> con;

	public IntSelectionButton(FontRenderer renderer, int xIn, int yIn, int widthIn, String msg) {
		super(xIn, yIn, widthIn, 24, msg);
		this.renderer = renderer;
		if(renderer.getStringWidth(msg) > 15) {
			textAbove = true;
		}else {
			textAbove = false;
		}
		
		int stringWidth = renderer.getStringWidth(msg);
		
		textFieldWidget = new TextFieldWidget(renderer, xIn + (textAbove?0:stringWidth), yIn + (textAbove ? 10 : 0), widthIn - 33-(textAbove?0:stringWidth), 24, null, "");
		textFieldWidget.setEnabled(true);
		textFieldWidget.setMaxStringLength(8);
		upButton = new LibmaticaButton(xIn + widthIn - 32, yIn-2 + (textAbove ? 10 : 0), 20, 14, "^", (b) -> {
			value++;
			updateValue(true);
		});
		downButton = new LibmaticaButton(xIn + widthIn - 32, yIn + 12 + (textAbove ? 10 : 0), 20, 13, "\\/", (b) -> {
			value--;
			updateValue(true);
		});
		setFocused(true);
	}
	
	
	
	public int getHight() {
		return textFieldWidget.getHeight() + (textAbove ? 10 : 0);
	}
	
	public void setValueListener(Consumer<Integer> con) {
		this.con = con;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int i) {
		value = i;
		updateValue(false);
	}
	
	
	private void updateValue(boolean user) {
		textFieldWidget.setText(value + "");
		if(con != null && user) {
			con.accept(value);
		}
	}

	
	
	
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		if(textAbove) {
		renderer.drawString(getMessage(), this.x, this.y, 0xffffff);
		}else {
			renderer.drawString(getMessage(), x, y+(height/2)-renderer.FONT_HEIGHT/2, 0xffffff);
		}
		textFieldWidget.active = active;
		textFieldWidget.render(p_render_1_, p_render_2_, p_render_3_);
		upButton.render(p_render_1_, p_render_2_, p_render_3_);
		downButton.render(p_render_1_, p_render_2_, p_render_3_);
	}
	
	@Override
	public boolean keyPressed(int p_keyPressed_1_, int p_keyPressed_2_, int p_keyPressed_3_) {
		boolean b = textFieldWidget.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
		boolean suc = false;
		try {
			Integer i = Integer.parseInt(textFieldWidget.getText());
			value = i;
			suc = true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(suc && b && p_keyPressed_1_ != 262 && p_keyPressed_1_ != 263)
		updateValue(true);
		return b;
	}

	@Override
	public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
		boolean b = textFieldWidget.charTyped(p_charTyped_1_, p_charTyped_2_);
		boolean suc = false;
		try {
			Integer i = Integer.parseInt(textFieldWidget.getText());
			value = i;
			suc = true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(suc && b)
		updateValue(true);
		return true;
	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		
		if(upButton.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_) ||
		downButton.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_) ||
		textFieldWidget.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_)) {
			return true;
		}
		return false;
	}

}
