package de.starmixcraft.libmatica.gui.wadgets;


import java.awt.Color;
import java.util.function.Consumer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;

public class BlockPosSelectionButton extends Widget{

	private IntSelectionButton xButton;
	private IntSelectionButton yButton;
	private IntSelectionButton zButton;
	
	private LibmaticaButton button;
	
	private MutableBlockPos blockPos;
	private FontRenderer renderer;
	
	private Consumer<BlockPos> con;
	public BlockPosSelectionButton(FontRenderer renderer, int xIn, int yIn, int width, int hight, String msg) {
		super(xIn, yIn, width, hight, msg);
		this.renderer = renderer;
		
		xButton = new IntSelectionButton(renderer, xIn, yIn+renderer.FONT_HEIGHT+2, width, "X:");
		yButton = new IntSelectionButton(renderer, xIn, yIn+renderer.FONT_HEIGHT+xButton.getHeight()+8, width, "Y:");
		zButton = new IntSelectionButton(renderer, xIn, yIn+renderer.FONT_HEIGHT+xButton.getHeight()+yButton.getHeight()+14, width, "Z:");
		button = new LibmaticaButton(xIn, yIn+renderer.FONT_HEIGHT+xButton.getHeight()+yButton.getHeight()+14+28, width-12, "Player Pos", (b)->{
			setBlockPos(Minecraft.getInstance().player.getPosition());
			updateValues(0);
		});
		xButton.setValueListener(this::updateValues);
		yButton.setValueListener(this::updateValues);
		zButton.setValueListener(this::updateValues);
		blockPos = new MutableBlockPos(0,0,0);
	}
	
	public void setUpdateListener(Consumer<BlockPos> pos) {
		this.con = pos;
	}
	
	
	public void setBlockPos(BlockPos pos) {
		this.blockPos.setPos(pos.getX(), pos.getY(), pos.getZ());
		xButton.setValue(blockPos.getX());
		yButton.setValue(blockPos.getY());
		zButton.setValue(blockPos.getZ());	
	}
	
	private void updateValues(int ignore) {
		blockPos.setPos(xButton.getValue(), yButton.getValue(), zButton.getValue());
		xButton.setValue(blockPos.getX());
		yButton.setValue(blockPos.getY());
		zButton.setValue(blockPos.getZ());
		if(con != null) {
			con.accept(getBlockPos());
		}
	}
	
	public BlockPos getBlockPos() {
		return blockPos.toImmutable();
	}
	
	@Override
	public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_) {
	}
	
	
	@Override
	public boolean keyPressed(int p_keyPressed_1_, int p_keyPressed_2_, int p_keyPressed_3_) {
		xButton.keyPressed(p_keyPressed_1_, p_keyPressed_2_,p_keyPressed_3_);
		yButton.keyPressed(p_keyPressed_1_, p_keyPressed_2_,p_keyPressed_3_);
		zButton.keyPressed(p_keyPressed_1_, p_keyPressed_2_,p_keyPressed_3_);
		button.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
		return super.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderer.drawString(getMessage(), x, y, Color.WHITE.getRGB());
		xButton.render(p_render_1_, p_render_2_, p_render_3_);
		yButton.render(p_render_1_, p_render_2_, p_render_3_);
		zButton.render(p_render_1_, p_render_2_, p_render_3_);
		button.render(p_render_1_, p_render_2_, p_render_3_);
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}
	
	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		if(xButton.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_) ||
				yButton.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_) ||
				zButton.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_) || button.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_)) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
		xButton.charTyped(p_charTyped_1_, p_charTyped_2_);
		yButton.charTyped(p_charTyped_1_, p_charTyped_2_);
		zButton.charTyped(p_charTyped_1_, p_charTyped_2_);
		button.charTyped(p_charTyped_1_, p_charTyped_2_);
		return true;
	}
	
	

}
