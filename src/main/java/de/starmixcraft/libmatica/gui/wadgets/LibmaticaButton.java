package de.starmixcraft.libmatica.gui.wadgets;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.math.MathHelper;

public class LibmaticaButton extends Button {

	public LibmaticaButton(int x, int y, int width, String text,
			IPressable onPress) {
		this(x, y, width, 20, text, onPress);
	}
	
	public LibmaticaButton(int x, int y, int width, int height, String text,
			IPressable onPress) {
		super(x, y, width, height, text, onPress);
	}
	
	
	
	@Override
	public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_) {
		 Minecraft minecraft = Minecraft.getInstance();
	      FontRenderer fontrenderer = minecraft.fontRenderer;
	      minecraft.getTextureManager().bindTexture(WIDGETS_LOCATION);
	      GlStateManager.color4f(1.0F, 1.0F, 1.0F, this.alpha);
	      int i = this.getYImage(this.isHovered());
	      GlStateManager.enableBlend();
	      GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
	      GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
	      this.blit(this.x, this.y, 0, 46 + i * 20, this.width / 2, this.height);
	      this.blit(this.x + this.width / 2, this.y, 200 - this.width / 2, 46 + i * 20, this.width / 2, this.height);
	      this.renderBg(minecraft, p_renderButton_1_, p_renderButton_2_);
	      //bufferBuilder.
	      
	      
	      int j = getFGColor();

	      this.drawCenteredString(fontrenderer, getMessage(), this.x + this.width / 2, this.y + (this.height - 8) / 2, j | MathHelper.ceil(this.alpha * 255.0F) << 24);

	}

}
