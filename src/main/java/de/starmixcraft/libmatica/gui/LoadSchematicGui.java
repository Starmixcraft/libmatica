package de.starmixcraft.libmatica.gui;

import java.io.File;
import java.io.IOException;

import de.starmixcraft.libmatica.Libmatica;
import de.starmixcraft.libmatica.gui.list.SchemListItem;
import de.starmixcraft.libmatica.gui.wadgets.LibmaticaButton;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.util.Util;
import net.minecraft.util.text.StringTextComponent;

public class LoadSchematicGui extends ExtendedList<SchemListItem> {

	
	
	
	private static boolean loading;
	
	public LoadSchematicGui(Minecraft mcIn, int widthIn, int heightIn, int topIn, int bottomIn, int slotHeightIn) {
		super(mcIn, widthIn, heightIn, topIn, bottomIn, slotHeightIn);
		for(File f : Libmatica.schemFileManager.listSchems()) {
			addEntry(new SchemListItem(f));
		}
	}
	
	

	
	public static class GuiScreen extends Screen{

		private LoadSchematicGui loadSchematicGui;
		public GuiScreen() {
			super(new StringTextComponent("test"));
		}
		@Override
		protected void init() {
			if(!loading) {
			loadSchematicGui = new LoadSchematicGui(getMinecraft(), (int) width, height, 10, height-30, 40);
			children.add(loadSchematicGui);
			addButton(new LibmaticaButton(width/2-152, height-25, 150, "Open Schem Folder", (b)->{
					 Util.getOSType().openFile(Libmatica.schemFileManager.getSchemFolder());
			}));
			}
			super.init();
		}
		
		@Override
		public void render(int x, int y, float p) {
			if(!loading) {
			loadSchematicGui.render(x, y, p);
			}else {
				renderBackground();
				RenderUtils.renderStringCentered(font, width/2, height/2, "Loading...", RenderUtils.RED, 2);
			}
			super.render(x, y, p);
		}
		
	}
	
	
	
	public static void loadFile(String name) {
		loading = true;
		Thread t = new Thread(()->{
			Minecraft.getInstance().ingameGUI.getChatGUI()
			.printChatMessage(new StringTextComponent("Loading Schematic, " + name));
			RenderabelSchematic schem = Libmatica.schemFileManager.loadSchem(name);
			schem.move(Minecraft.getInstance().player.getPosition());
			schem.setInstance();
			Minecraft.getInstance().displayGuiScreen(new LibmaticaMainGui());	
			loading = false;
		});
		t.setName("SchemLoader Thread");
		t.start();
		Minecraft.getInstance().displayGuiScreen(null);
	}

	@Override
	public boolean mouseScrolled(double x, double y, double direction) {
		return super.mouseScrolled(x, y, direction);
	}
}
