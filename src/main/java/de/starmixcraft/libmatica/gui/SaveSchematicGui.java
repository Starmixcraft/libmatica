package de.starmixcraft.libmatica.gui;

import java.util.ArrayList;

import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import de.starmixcraft.libmatica.Libmatica;
import de.starmixcraft.libmatica.exception.ToSmallSchemException;
import de.starmixcraft.libmatica.gui.wadgets.BlockPosSelectionButton;
import de.starmixcraft.libmatica.gui.wadgets.LibmaticaButton;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import de.starmixcraft.libmatica.schematic.io.ISchematicIOProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.CheckboxButton;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;

public class SaveSchematicGui extends Screen {

	public static boolean showBounds;
	public static BlockPos pos1 = new BlockPos(0, 0, 0);
	public static BlockPos pos2 = new BlockPos(0, 0, 0);

	private TextFieldWidget name;

	private ArrayList<ISchematicIOProvider> providers; 
	
	public SaveSchematicGui() {
		super(new StringTextComponent("Save Schematic"));
		providers = new ArrayList<>();

	}

	@Override
	protected void init() {
		providers.addAll(Libmatica.schemFileManager.listProviders());
		BlockPosSelectionButton pos1Button = new BlockPosSelectionButton(font, width / 2 - 120, height / 2 - 100, 120,
				32, "First Position");
		pos1Button.setBlockPos(pos1);
		pos1Button.setUpdateListener((pos) -> {
			pos1 = pos;
		});
		addButton(pos1Button);

		BlockPosSelectionButton pos2Button = new BlockPosSelectionButton(font, width / 2 + 5, height / 2 - 100, 120, 32,
				"Second Position");
		pos2Button.setBlockPos(pos2);
		pos2Button.setUpdateListener((pos) -> {
			pos2 = pos;
		});
		addButton(pos2Button);
		CheckboxButton toggleWidget = new CheckboxButton(10, 10, 120, 20, "Show bounds", showBounds) {
			@Override
			public void onPress() {
				super.onPress();
				showBounds = this.func_212942_a();
			}
		};
		addButton(toggleWidget);
		
		name = new TextFieldWidget(font, width / 2 - 150, height / 2 + 30, 300, 20, "Schematic Name");
		name.setText("name");
		addButton(name);
		
		addButton(new LibmaticaButton(width / 2 - 150, height / 2 + 98, 300, 20, getSelectedProvider().getName(), (b)->{
			selectedProviderIndex++;
			if(selectedProviderIndex >= providers.size()) {
				selectedProviderIndex = 0;
			}
			b.setMessage(getSelectedProvider().getName());
		}));
		
		Button save = new Button(width / 2 - 150, height / 2 + 68, 300, 20, "Save", (b) -> {

			if (!name.getText().endsWith(".schem")) {
				name.setText(name.getText() + ".schem");
			}
			b.active = false;
			b.setMessage("saving...");
			new Thread(() -> {
				try {
					RenderabelSchematic renderabelSchematic = RenderabelSchematic
							.createSchem(Minecraft.getInstance().world, pos1, pos2);
					try {
						Libmatica.schemFileManager.saveSchematic(name.getText(), renderabelSchematic,
								getSelectedProvider());

					} catch (DuplicateName e) {
						b.setMessage("Save Failed... name allready used");
						return;
					}
					Minecraft.getInstance().ingameGUI.getChatGUI().printChatMessage(new StringTextComponent("Done, Saved schem " + name.getText()));
					name.setText("Schematic Name");
					if (toggleWidget.func_212942_a()) {
						toggleWidget.onPress();
					}
				} catch (ToSmallSchemException e) {
					b.setMessage("Save Failed... To Small Aria");
				}
			}).start();

		});
		addButton(save);
		super.init();
	}

	
	
	public int selectedProviderIndex;
	public ISchematicIOProvider getSelectedProvider() {
		return providers.get(selectedProviderIndex);
	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		renderBackground();
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

}
