package de.starmixcraft.libmatica.schematic;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;

public class RenderabelBlock {

	private BlockState state;
	
	private BlockPos inschem;	
	private RenderabelSchematic schematic;
	
	public RenderabelBlock(BlockState state, BlockPos inschem, RenderabelSchematic schem) {
		this.schematic = schem;
		this.inschem = inschem;
		this.state = state;
		
	}
	
	
	public BlockPos getRelativePos() {
		return inschem;
	}
	/**
	 * gets the relative in schem pos
	 * @return
	 */
	public int inSchemX() {
		return inschem.getX();
	}
	/**
	 * gets the relative in schem pos
	 * @return
	 */
	public int inSchemY() {
		return inschem.getY();
	}
	/**
	 * gets the relative in schem pos
	 * @return
	 */
	public int inSchemZ() {
		return inschem.getZ();
	}
	
	
	public void rotate(Rotation rotation) {
		state = state.rotate(rotation);
	}
	
	/**
	 * gets the blockstate to be renderd
	 * @return
	 */
	public BlockState getBlockStateToRender() {
		return state;
	}
	/**
	 * gets the absolut world position of this block
	 * @return
	 */
	public BlockPos getAbsolutePos() {
		return schematic.getAbsolutLocation(this);
	}
}
