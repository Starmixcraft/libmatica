package de.starmixcraft.libmatica.schematic;

import java.util.concurrent.locks.ReentrantLock;

import de.starmixcraft.libmatica.exception.ToSmallSchemException;
import de.starmixcraft.libmatica.gui.LibmaticaMainGui;
import de.starmixcraft.libmatica.renderer.BoxRenderer;
import de.starmixcraft.libmatica.renderer.SchematicRenderer;
import de.starmixcraft.libmatica.renderer.filter.SchematicFilter;
import de.starmixcraft.libmatica.schematic.SchematicMaterialList.BlockCount;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.fluid.IFluidState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IEnviromentBlockReader;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;

public class RenderabelSchematic implements IEnviromentBlockReader {

	// y - x - z
	private RenderabelBlock[][][] blocks;

	private BlockPos schemPos;
	private BlockPos newSchemPos;

	public static RenderabelSchematic INSTANCE;

	private static SchematicRenderer renderer = new SchematicRenderer();

	private Direction direction;

	public BlockRayTraceResult mouseOver;

	public String name;

	private ReentrantLock renderModifyLock;
	
	private SchematicFilter filter;
	
	
	private int ySize, xSize, zSize;

	public SchematicRenderer getRenderer() {
		return renderer;
	}

	@SuppressWarnings("unused")
	public RenderabelSchematic(BlockPos schemPos, int ySize, int xSize, int zSize) {
		this.blocks = new RenderabelBlock[ySize][xSize][zSize];
		this.ySize = ySize;
		this.xSize = xSize;
		this.zSize = zSize;
		this.schemPos = schemPos;
		renderModifyLock = new ReentrantLock();
		direction = Direction.SOUTH;
		if (true) {// debug
			BlockState s = Blocks.STONE.getDefaultState();
			setBlockStateRelative(new BlockPos(0, 0, 0), s);
			setBlockStateRelative(new BlockPos(0, ySize - 1, 0), s);

			setBlockStateRelative(new BlockPos(xSize - 1, 0, 0), s);
			setBlockStateRelative(new BlockPos(xSize - 1, ySize - 1, 0), s);

			setBlockStateRelative(new BlockPos(0, 0, zSize - 1), s);
			setBlockStateRelative(new BlockPos(0, ySize - 1, zSize - 1), s);

			setBlockStateRelative(new BlockPos(xSize - 1, 0, zSize - 1), s);
			setBlockStateRelative(new BlockPos(xSize - 1, ySize - 1, zSize - 1), s);
			int i = 0;
			for (int y = 0; y < blocks.length; y++) {
				for (int x = 0; x < blocks[y].length; x++) {
					for (int z = 0; z < blocks[y][x].length; z++) {
						BlockState state = Block.getStateById(i++);
						setBlockStateRelative(new BlockPos(x, y, z), state);
					}
				}
			}
		}

	}

	public void setInstance() {
		INSTANCE = this;
		reload = true;
	}

	/**
	 * Returns a material list for the current schem
	 * 
	 * @return
	 */
	public SchematicMaterialList takeSnapshot() {
		SchematicMaterialList snapshot = new SchematicMaterialList();
		for (int y = 0; y < blocks.length; y++) {
			for (int x = 0; x < blocks[y].length; x++) {
				for (int z = 0; z < blocks[y][x].length; z++) {
					RenderabelBlock block = blocks[y][x][z];
					Block b = block.getBlockStateToRender().getBlock();
					if(filter != null && !filter.shoodRenderBlock(block.getRelativePos(), block.getAbsolutePos())) {
						continue;
					}
					if (b == Blocks.AIR || b == Blocks.VOID_AIR || b == Blocks.CAVE_AIR) {
						continue;
					}
					BlockCount c = snapshot.getCount(b);
					c.incrementNeeded();
					if (Minecraft.getInstance().world.getBlockState(block.getAbsolutePos()).getBlock() == b) {
						c.incrementGot();
					}
				}
			}
		}
		return snapshot;
	}

	public void move(BlockPos newpos) {
		schemPos = newpos;
		flagAllChunksInSchemDirty(true);
	}

	boolean reload;

	/**
	 * Flags all chunks for buffer rebuild on large schems this will take a long
	 * time
	 */
	public void flagAllChunksInSchemDirty(boolean extendedY) {
		renderModifyLock.lock();
		try {
			int startx = schemPos.getX() >> 4;
			int startz = schemPos.getZ() >> 4;
			int endX = (schemPos.getX()+getXsize() >> 4) + 1;
			int endZ = (schemPos.getZ()+getZsize() >> 4) + 1;
			int ymin = schemPos.getY() >> 4;
			int ymax = ((schemPos.getY() + getHeight()) >> 4) + 1;
			if (extendedY) {
				if (ymin > 0) {
					ymin--;
				}
				if (ymax < 15) {
					ymax++;
				}
			}
			renderer.flagDirty();
			for (int x = startx; x < endX; x++) {
				for (int z = startz; z < endZ; z++) {
					for (int i = ymin; i < ymax; i++) {
						renderer.flagPosDirty(new BlockPos(x << 4, i << 4, z << 4));
					}
				}
			}
		} finally {
			renderModifyLock.unlock();
		}
	}

	/**
	 * Rendes the schematic
	 */
	public void render() {
		renderModifyLock.lock();
		try {

			
			if (reload || newSchemPos != null) {
				reload = false;
				if (newSchemPos != null) {
					schemPos = newSchemPos;
					newSchemPos = null;
				}
				renderer.clear();
				flagAllChunksInSchemDirty(false);
			}


			if (LibmaticaMainGui.renderSchem)
				renderer.render();

			Minecraft mc = Minecraft.getInstance();
			ClientPlayerEntity entity = mc.player;
			double d0 = (double) mc.playerController.getBlockReachDistance();
			Vec3d vec3d = entity.getEyePosition(1);
			Vec3d vec3d1 = entity.getLook(1);
			Vec3d vec3d2 = vec3d.add(vec3d1.x * d0, vec3d1.y * d0, vec3d1.z * d0);
			mouseOver = rayTraceBlocks(new RayTraceContext(vec3d, vec3d2, RayTraceContext.BlockMode.OUTLINE,
					RayTraceContext.FluidMode.ANY, entity));
		} finally {
			renderModifyLock.unlock();
		}

	}

	/**
	 * Renders the debug boxes
	 */
	public void renderBox() {
		renderModifyLock.lock();
		try {
				BoxRenderer.renderBounds(schemPos.getX(), schemPos.getY(), schemPos.getZ(),
						schemPos.getX() + getXsize(), schemPos.getY() + getHeight(), schemPos.getZ() + getZsize());

			if (LibmaticaMainGui.renderDebugBoxes)
				renderer.renderBox();
		} finally {
			renderModifyLock.unlock();
		}
	}
	
	
	public void setFilter(SchematicFilter filter) {
		this.filter = filter;
		flagAllChunksInSchemDirty(false);
	}
	
	public SchematicFilter getFilter(){
		return filter;
	}

	public void unload() {
		INSTANCE = null;
	}

	/**
	 * Returns hight of the schematic
	 * 
	 * @return
	 */
	public int getHeight() {
		return ySize;
	}

	/**
	 * Returns size in x direction of schematic
	 * 
	 * @return
	 */
	public int getXsize() {
		return xSize;
	}

	/**
	 * Returns size in z of the schematic
	 * 
	 * @return
	 */
	public int getZsize() {
		return zSize;
	}

	/**
	 * Sets a blockstate in the schematic with relative pos
	 * 
	 * @param relativePos
	 * @param set
	 */
	public void setBlockStateRelative(BlockPos relativePos, BlockState set) {
		if (!isInSchemRelative(relativePos)) {
			return;
		}

		blocks[relativePos.getY()][relativePos.getX()][relativePos.getZ()] = new RenderabelBlock(set, relativePos,
				this);
	}

	/**
	 * Sets a blockstate in the schematic with absolut pos
	 * 
	 * @param worldPos
	 * @param set
	 */
	public void setBlockState(BlockPos worldPos, BlockState set) {
		if (!isInSchem(worldPos)) {
			System.out.println("Blockpos not in schem");
			return;
		}
		BlockPos relativePos = new BlockPos(worldPos.getX() - schemPos.getX(), worldPos.getY() - schemPos.getY(),
				worldPos.getZ() - schemPos.getZ());
		setBlockStateRelative(relativePos, set);
	}

	/**
	 * Currenty unsuported because the the getBlockAbsulute and getBlockRelative are
	 * not transformed
	 * 
	 * @param rotation
	 */
	@Deprecated
	public void rotate(Rotation rotation) {
		renderModifyLock.lock();
		try {
			if (rotation == Rotation.NONE) {
				return;
			}

			rotateBlockModels(rotation);
			this.direction = rotation.rotate(this.direction);
			flagAllChunksInSchemDirty(false);
		} finally {
			renderModifyLock.unlock();
		}
	}

	private void rotateBlockModels(Rotation rot) {
		for (int y = 0; y < blocks.length; y++) {
			for (int x = 0; x < blocks[y].length; x++) {
				for (int z = 0; z < blocks[y][x].length; z++) {
					blocks[y][x][z].rotate(rot);
				}
			}
		}
	}

	/**
	 * Gets the worldPositon of a RenderabelBlock
	 * 
	 * @param block
	 * @return BlockPos
	 */
	public BlockPos getAbsolutLocation(RenderabelBlock block) {
		BlockPos abBlockPos = new BlockPos(schemPos).add(block.inSchemX(), block.inSchemY(), block.inSchemZ());
		if(!isInSchem(abBlockPos)) {
			throw new IllegalAccessError("Block at " + block.inSchemX() + " " + block.inSchemY() + " " + block.inSchemZ() + " is not in schem in world with blockpos " + abBlockPos);
		}
		return abBlockPos;
	}

	public BlockPos getRelativeBlockPos(BlockPos worldpos) {
		return new BlockPos(worldpos.getX() - schemPos.getX(), worldpos.getY() - schemPos.getY(),
				worldpos.getZ() - schemPos.getZ());
	}

	/**
	 * Returns the RenderabelBlock at the absolut world position or null
	 * 
	 * @param pos
	 * @return {@link RenderabelBlock}
	 */
	public RenderabelBlock getBlock(BlockPos worldPos) {
		RenderabelBlock b = getBlockRelative(getRelativeBlockPos(worldPos));
		
		if(b != null && !b.getAbsolutePos().equals(worldPos)) {
			throw new IllegalAccessError(worldPos.toString() + " returned block for pos " + b.getAbsolutePos().toString());
		}
		return b;
	}

	/**
	 * Returns the RenderabelBlock at the relative world position or null
	 * 
	 * @param pos
	 * @return {@link RenderabelBlock}
	 */
	public RenderabelBlock getBlockRelative(BlockPos blockpos) {
		if (!isInSchemRelative(blockpos)) {
			return null;
		}
		return blocks[blockpos.getY()][blockpos.getX()][blockpos.getZ()];
	}

	/**
	 * Checks if the world positon is inside
	 * 
	 * @param worldPos
	 * @return {@link Boolean}
	 */
	public boolean isInSchem(BlockPos worldPos) {
		if (worldPos.getX() >= schemPos.getX() && worldPos.getY() >= schemPos.getY()
				&& worldPos.getZ() >= schemPos.getZ() && worldPos.getX() < (schemPos.getX() + getXsize()) && worldPos.getY() < (schemPos.getY() + getHeight())
				&& worldPos.getZ() < (schemPos.getZ() + getZsize())) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the position relative to this schematic is inside
	 * 
	 * @param worldPos
	 * @return {@link Boolean}
	 */
	public boolean isInSchemRelative(BlockPos relativePos) {
		if (relativePos.getX() >= 0 && relativePos.getY() >= 0 && relativePos.getZ() >= 0 && relativePos.getX() < getXsize() && relativePos.getY() < getHeight() && relativePos.getZ() < getZsize()) {
			return true;
		}
		return false;
	}

	@Override
	public TileEntity getTileEntity(BlockPos pos) {
		return null;
	}

	@Override
	public BlockState getBlockState(BlockPos pos) {
		if (isInSchem(pos)) {
			RenderabelBlock block = getBlock(pos);
			
			if (block == null || (filter != null && block != null && !filter.shoodRenderBlock(block.getRelativePos(), block.getAbsolutePos()))) {
				return Blocks.AIR.getDefaultState(); // Fix nullpointer
			}
			return block.getBlockStateToRender();
		}
		return Blocks.AIR.getDefaultState();
	}

	@Override
	public IFluidState getFluidState(BlockPos pos) {
		// TODO Auto-generated method stub
		return getBlockState(pos).getFluidState();
	}

	public BlockPos getSchemPos() {
		return schemPos;
	}

	@Override
	public Biome getBiome(BlockPos pos) {
		// TODO Auto-generated method stub
		return Biomes.PLAINS;
	}

	@Override
	public int getLightFor(LightType type, BlockPos pos) {
		return 15;
	}
	
	public int getArea() {
		return getHeight() * getXsize() * getZsize();
	}

	public static RenderabelSchematic createSchem(World world, BlockPos pos1, BlockPos pos2) throws ToSmallSchemException {
		BlockPos min = new BlockPos(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()),
				Math.min(pos1.getZ(), pos2.getZ()));
		BlockPos max = new BlockPos(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()),
				Math.max(pos1.getZ(), pos2.getZ()));
		int xSize = max.getX() - min.getX();
		int ySize = max.getY() - min.getY();
		int zSize = max.getZ() - min.getZ();
		if (xSize <= 0 || ySize <= 0 || zSize <= 0) {
			 throw new ToSmallSchemException();
		}
		RenderabelSchematic schematic = new RenderabelSchematic(min, ySize + 1, xSize + 1, zSize + 1);
		double blocks = schematic.getArea();
		int lastUpdate = 0;
		int progress = 0;
		double done = 0;
		for (int y = min.getY(); y <= max.getY(); y++) {
			for (int x = min.getX(); x <= max.getX(); x++) {
				for (int z = min.getZ(); z <= max.getZ(); z++) {
					BlockPos pos = new BlockPos(x, y, z);
					schematic.setBlockState(pos, world.getBlockState(pos));
					done++;
					progress = (int)((done/blocks)*50D);
					if(lastUpdate < progress) {
						lastUpdate = progress;
						Minecraft.getInstance().ingameGUI.getChatGUI().printChatMessage(new StringTextComponent("Saving schem, Progress: " + progress + "%"));
					}
				}
			}
		}
		return schematic;
	}
}
