package de.starmixcraft.libmatica.schematic;

import java.util.HashMap;
import java.util.Iterator;

import net.minecraft.block.Block;

/**
 * This class allows to list all needed resurces for a schematic
 * @author lucas
 *
 */
public class SchematicMaterialList {

	private HashMap<Block, BlockCount> blockCounts;
	
	public SchematicMaterialList() {
		blockCounts = new HashMap<>();
	}
	
	public Iterator<Block> getBlockItterator(){
		return blockCounts.keySet().iterator();
	}
	
	public BlockCount getCount(Block b) {
		if(blockCounts.containsKey(b)) {
			return blockCounts.get(b);
		}
		BlockCount a= new BlockCount();
		blockCounts.put(b, a);
		return a;
	}
	

	
	
	
	
	
	
	public static class BlockCount{
		private int needed;
		private int got;
		
		public BlockCount() {
			// TODO Auto-generated constructor stub
		}
		
		
		public void incrementNeeded() {
			needed++;
		}
		public void incrementGot() {
			got++;
		}
		
		public int getNeeded() {
			return needed;
		}
		public int getGot() {
			return got;
		}
		
		public int getMissing() {
			return needed - got;
		}
	}
}
