package de.starmixcraft.libmatica.schematic.io;

import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.nbt.CompoundNBT;

public interface ISchematicIOProvider {
	
	public CompoundNBT saveSchem(RenderabelSchematic schematic);
	
	public RenderabelSchematic loadSchem(CompoundNBT nbt);
	
	public String getName();
}
