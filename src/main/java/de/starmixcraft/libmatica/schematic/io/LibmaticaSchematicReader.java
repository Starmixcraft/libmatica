package de.starmixcraft.libmatica.schematic.io;

import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.block.Block;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;

public class LibmaticaSchematicReader implements ISchematicIOProvider{

	@Override
	public CompoundNBT saveSchem(RenderabelSchematic schematic) {
		CompoundNBT schem = new CompoundNBT();
		schem.putIntArray("size", new int[] {schematic.getXsize(), schematic.getHeight(), schematic.getZsize()});
		int[] blocks = new int[schematic.getArea()];
		for (int x = 0; x < schematic.getXsize(); x++) {
			for (int y = 0; y < schematic.getHeight(); y++) {
				for (int z = 0; z < schematic.getZsize(); z++) {
					putInt(blocks, x, y, z, schematic.getXsize(), schematic.getHeight(), schematic.getZsize(), Block.getStateId(schematic.getBlockRelative(new BlockPos(x, y, z)).getBlockStateToRender()));
				}
			}
		}
		schem.putIntArray("blocks", blocks);
		return schem;
	}
	
	
	
	
	private static void putInt(int[] data, int x, int y, int z, int xsize, int ysize, int zsize, int in) {
		data[x*ysize*zsize + y*zsize + z] = in;
	}
	private static int getInt(int[] data, int x, int y, int z, int xsize, int ysize, int zsize) {
		return data[x*ysize*zsize+y*zsize+z];
	}

	@Override
	public RenderabelSchematic loadSchem(CompoundNBT nbt) {
		int[] size = nbt.getIntArray("size");
		RenderabelSchematic schematic = new RenderabelSchematic(new BlockPos(0, 0, 0), size[1], size[0], size[2]);
		int[] blocks = nbt.getIntArray("blocks");
		for (int x = 0; x < schematic.getXsize(); x++) {
			for (int y = 0; y < schematic.getHeight(); y++) {
				for (int z = 0; z < schematic.getZsize(); z++) {
					schematic.setBlockStateRelative(new BlockPos(x, y, z), Block.getStateById(getInt(blocks, x, y, z, schematic.getXsize(), schematic.getHeight(), schematic.getZsize())));
				}
			}
		}
		return schematic;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "libmatica";
	}

}
