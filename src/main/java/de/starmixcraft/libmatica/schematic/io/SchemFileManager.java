package de.starmixcraft.libmatica.schematic.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;

public class SchemFileManager {

	private File schemfolder;
	
	
	public static ISchematicIOProvider defaultProvider = new McSchematicReader();	
	private HashMap<String, ISchematicIOProvider> providers;

	public SchemFileManager() {
		schemfolder = new File("libmatica/schematics");
		schemfolder.mkdirs();
		providers = new HashMap<>();
		addProvider(new LibmaticaSchematicReader());
		addProvider(defaultProvider);
	}
	
	
	public Collection<ISchematicIOProvider> listProviders() {
	return providers.values();
	}
	
	private void addProvider(ISchematicIOProvider provider) {
		providers.put(provider.getName(), provider);
	}
	public File getSchemFolder() {
		return schemfolder;
	}

	public ArrayList<File> listSchems() {
		ArrayList<File> files = new ArrayList<>();
		for (File f : schemfolder.listFiles()) {
			if (f.isFile()) {
				files.add(f);
			}
		}
		return files;
	}
	
	
	public ISchematicIOProvider getSchemType(CompoundNBT nbt) {
		if(nbt.contains("format")) {
			String name = nbt.getString("format");
			if(providers.containsKey(name)) {
				return providers.get(name);
			}else {
				return defaultProvider;
			}
		}else {
			return defaultProvider;
		}
	}
	
	
	public RenderabelSchematic loadSchem(String name) {
		try {
			CompoundNBT nbt = CompressedStreamTools.read(getFile(name));
			ISchematicIOProvider prov = getSchemType(nbt);
			return prov.loadSchem(nbt);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Exception saveSchematic(String name, RenderabelSchematic schem, ISchematicIOProvider provider) throws DuplicateName {
		if(getFile(name).exists()) {
			throw new DuplicateName();
		}
		try {
			CompoundNBT nbt = provider.saveSchem(schem);
			if(providers.containsValue(provider)) {
				String format = "";
				for(Entry<String, ISchematicIOProvider> a : providers.entrySet()) {
					if(a.getValue() == provider) {
						format = a.getKey();
					}
				}
				nbt.putString("format", format);
			}
			CompressedStreamTools.write(nbt, getFile(name));
		} catch (IOException e) {
			return e;
		}
		return null;
	}
	
	
	private File getFile(String name) {
		return new File(schemfolder, name);
	}
}
