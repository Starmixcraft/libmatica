package de.starmixcraft.libmatica.schematic.io;

import java.lang.reflect.Field;
import java.util.List;

import com.google.common.collect.Lists;

import de.starmixcraft.libmatica.schematic.RenderabelBlock;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;

public class McSchematicReader implements ISchematicIOProvider {

	@Override
	public CompoundNBT saveSchem(RenderabelSchematic schematic) {
		Template clipboard = new Template();
		BlockPos size = new BlockPos(schematic.getXsize(), schematic.getHeight(), schematic.getZsize());
		BlockPos startPos = schematic.getSchemPos();
		if (size.getX() >= 1 && size.getY() >= 1 && size.getZ() >= 1) {
			BlockPos blockpos = startPos.add(size).add(-1, -1, -1);
			List<Template.BlockInfo> list = Lists.newArrayList();
			List<Template.BlockInfo> list1 = Lists.newArrayList();
			List<Template.BlockInfo> list2 = Lists.newArrayList();
			BlockPos blockpos1 = new BlockPos(Math.min(startPos.getX(), blockpos.getX()),
					Math.min(startPos.getY(), blockpos.getY()), Math.min(startPos.getZ(), blockpos.getZ()));
			BlockPos blockpos2 = new BlockPos(Math.max(startPos.getX(), blockpos.getX()),
					Math.max(startPos.getY(), blockpos.getY()), Math.max(startPos.getZ(), blockpos.getZ()));

			setField("size", clipboard, size);

			double reblocks = schematic.getArea();
			double done = 0;
			int lastProgress = 50;
			for (BlockPos blockpos3 : BlockPos.getAllInBoxMutable(blockpos1, blockpos2)) {
				BlockPos blockpos4 = blockpos3.subtract(blockpos1);
				RenderabelBlock block = schematic.getBlockRelative(blockpos4);
				BlockState blockstate = null;
				if (block != null) {
					blockstate = block.getBlockStateToRender();
				} else {
					done++;
					continue;
				}
				list.add(new Template.BlockInfo(blockpos4, blockstate, (CompoundNBT) null));
				done++;
				int progress = (int) ((done/reblocks) * 50D) + 50;
				if (lastProgress < progress) {
					lastProgress = progress;
					Minecraft.getInstance().ingameGUI.getChatGUI()
							.printChatMessage(new StringTextComponent("Saving schem, Progress: " + progress + "%"));
				}
			}

			List<Template.BlockInfo> list3 = Lists.newArrayList();
			list3.addAll(list);
			list3.addAll(list1);
			list3.addAll(list2);
			List<List<Template.BlockInfo>> blocks = Lists.newArrayList();
			blocks.add(list3);
			setField("blocks", clipboard, blocks);
			setField("entities", clipboard, Lists.newArrayList());
			return clipboard.writeToNBT(new CompoundNBT());
		}
		return null;
	}

	@Override
	public RenderabelSchematic loadSchem(CompoundNBT nbt) {
		Minecraft.getInstance().ingameGUI.getChatGUI()
		.printChatMessage(new StringTextComponent("Loading Schematic, Reading nbt tag"));

		Template clipboard = new Template();
		clipboard.read(nbt);
		RenderabelSchematic renderabelSchematic = new RenderabelSchematic(new BlockPos(0, 0, 0),
				clipboard.getSize().getY(), clipboard.getSize().getX(), clipboard.getSize().getZ());
				double reblocks = renderabelSchematic.getArea();
				System.out.println(reblocks);
				double done = 0;
				int lastProgress = 0;
		List<List<Template.BlockInfo>> blocks = getField("blocks", clipboard, List.class);
		for (List<BlockInfo> a : blocks) {
			for (BlockInfo b : a) {
				renderabelSchematic.setBlockStateRelative(b.pos, b.state);
				done++;
				int progess = (int) ((done/reblocks)*100D);
				if(lastProgress < progess) {
					lastProgress = progess;
					Minecraft.getInstance().ingameGUI.getChatGUI()
					.printChatMessage(new StringTextComponent("Loading Schematic, Progress: " + progess + "%"));
				}
			}
		}
		return renderabelSchematic;
	}
	
	@Override
	public String getName() {
		return "minecraft";
	}

	public <T> T getField(String name, Object obj, Class<T> clazz) {
		try {
			Field f = obj.getClass().getDeclaredField(name);
			f.setAccessible(true);
			Object a = f.get(obj);
			return clazz.cast(a);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setField(String name, Object obj, Object value) {
		try {
			Field f = obj.getClass().getDeclaredField(name);
			f.setAccessible(true);
			f.set(obj, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
