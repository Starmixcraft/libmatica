package de.starmixcraft.libmatica.renderer;

/**
 * Indecates the blocktint to be used
 * 
 * @author lucas
 *
 */
public enum BlockTint {

	WRONG_BLOCK(255, 0, 0, 0.8f), REQUIRE_AIR(255, 0, 255, 0.25f), WRONG_STATE(255, 255, 0, 0.6f), MISSING_BLOCK(200, 200, 255, 0.25f),
	ALLREADY_ALL_RIGHT(0, 0, 0, 0);

	private int r, g, b;
	private float tint;

	private BlockTint(int r, int g, int b, float tint) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.tint = tint;
	}

	public int getR() {
		return r;
	}

	public int getG() {
		return g;
	}

	public int getB() {
		return b;
	}
	public float getTint() {
		return tint;
	}

}
