package de.starmixcraft.libmatica.renderer;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import de.starmixcraft.libmatica.gui.LibmaticaMainGui;
import de.starmixcraft.libmatica.renderer.chunk.RenderChunk;
import net.minecraft.util.math.BlockPos;

public class SchematicRenderer {

	private ConcurrentHashMap<Long, RenderChunk> chunks;

	public SchematicRenderer() {
		chunks = new ConcurrentHashMap<>();
	}

	public boolean reloadedBuffer = false;

	long blockRenderTime = 0;
	long tileEntityRenderTime = 0;

	private boolean requireClear;

	/**
	 * Renders blocks to the world
	 */
	public void render() {
		reloadedBuffer = false;
		long start = System.nanoTime();
		Iterator<RenderChunk> chunks = this.chunks.values().iterator();
		while (chunks.hasNext()) {

			RenderChunk type = chunks.next();
			type.render();
			reloadedBuffer = false;
			if (type.isEmpty()) {
				chunks.remove();
			}
		}

		blockRenderTime = System.nanoTime() - start;

	}
	
	
	public void flagDirty() {
		for (RenderChunk c : chunks.values()) {
			c.flagDirty();
		}
	}

	public void renderTileEntitys() {
		long start = System.nanoTime();
		if (LibmaticaMainGui.renderTileEntitys)
			for (RenderChunk c : chunks.values()) {
				c.renderTileEntitys();
			}
		tileEntityRenderTime = System.nanoTime() - start;
	}

	public void clear() {
		for (RenderChunk c : chunks.values()) {
			c.clear();
		}
		chunks.clear();
	}

	/**
	 * Gets the chunk at the chunkcords
	 * 
	 * @param x chunkx
	 * @param z chunkz
	 * @return {@link RenderChunk}
	 */
	public RenderChunk getChunk(int x, int z) {
		long l = toLong(x, z);
		RenderChunk chunk = chunks.get(l);
		if (chunk == null) {
			chunk = new RenderChunk(x, z);
			chunks.put(l, chunk);
		}
		return chunk;
	}

	/**
	 * Gets the total ammount of non invisible blocks in the renderer
	 * 
	 * @return
	 */
	public int getTotalBlocks() {
		int j = 0;
		for (RenderChunk c : chunks.values()) {
			j += c.getTotalBlocks();
		}
		return j;
	}

	public int getTileEntitys() {
		int j = 0;
		for (RenderChunk c : chunks.values()) {
			j += c.getTileEntitys();
		}
		return j;
	}

	/**
	 * Gets the ammount of blocks that are drawen by the renderer eg visable
	 * 
	 * @return
	 */
	public int getDrawingBlocks() {
		int j = 0;
		for (RenderChunk c : chunks.values()) {
			j += c.getDrawingBlocks();
		}
		return j;
	}

	/**
	 * Gets the debug info for block renders
	 * 
	 * @return
	 */
	public String getDebugInfo() {
		return "BlockRenderer: ChunkCount: " + chunks.size() + ", MSR: " + getExactMSRBlocks() + ", TB: "
				+ getTotalBlocks() + ", RB: " + getDrawingBlocks();
	}

	public String getDebugInfoTileEntitys() {
		return "TileRenderer: Count: " + getTileEntitys() + ", MSR: " + getExactMSRTileEntitys();
	}

	/**
	 * Gets the time to render the schem in ms
	 * 
	 * @return
	 */
	public String getExactMSRBlocks() {
		double dif = 1000 * 1000;
		return (blockRenderTime > 10 * dif ? "�c" : "�a") + String.format("%.3f", blockRenderTime / dif);
	}

	/**
	 * Gets the time to render the tileentitys in ms
	 * 
	 * @return
	 */
	public String getExactMSRTileEntitys() {
		double dif = 1000 * 1000;
		return (tileEntityRenderTime > 10 * dif ? "�c" : "�a") + String.format("%.3f", tileEntityRenderTime / dif);
	}

	/**
	 * Flags a blockpos dirty for buffer rebuild
	 * 
	 * @param pos
	 */
	public void flagPosDirty(BlockPos pos) {
		if(pos.getY() < 0) { //fix ArrayIndexOutOfBoundsException
			return;
		}
		int x = pos.getX() >> 4;
		int z = pos.getZ() >> 4;
		RenderChunk chunk = getChunk(x, z);
		if(pos.getY() > 255) {//fix ArrayIndexOutOfBoundsException
			return;
		}
		chunk.getSubChunk(pos.getY() >> 4).flagDirty(); // flag the subchunk diry
	}

	/**
	 * Renders debug boxes arround every subchunk
	 */
	public void renderBox() {

		for (RenderChunk c : chunks.values()) {
			c.renderBox();
		}
	}

	/**
	 * Converts chunk cords to a long
	 * 
	 * @param x
	 * @param z
	 * @return
	 */
	public long toLong(int x, int z) {
		return x << 8 | z & 255;
	}
}
