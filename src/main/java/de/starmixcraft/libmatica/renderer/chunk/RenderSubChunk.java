package de.starmixcraft.libmatica.renderer.chunk;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.antlr.v4.runtime.atn.BlockStartState;
import org.lwjgl.opengl.GL11;

import com.mojang.authlib.BaseUserAuthentication;
import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.FogMode;

import de.starmixcraft.libmatica.config.RenderConfig;
import de.starmixcraft.libmatica.renderer.BlockRenderer;
import de.starmixcraft.libmatica.renderer.BlockTint;
import de.starmixcraft.libmatica.renderer.BoxRenderer;
import de.starmixcraft.libmatica.renderer.SchematicRenderer;
import de.starmixcraft.libmatica.renderer.filter.SchematicFilter;
import de.starmixcraft.libmatica.schematic.RenderabelBlock;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import de.starmixcraft.libmatica.schematic.io.SchemFileManager;
import io.netty.util.ResourceLeakException;
import net.minecraft.block.AbstractBannerBlock;
import net.minecraft.block.AbstractSkullBlock;
import net.minecraft.block.BannerBlock;
import net.minecraft.block.BedBlock;
import net.minecraft.block.BellBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ConduitBlock;
import net.minecraft.block.EndGatewayBlock;
import net.minecraft.block.EndPortalBlock;
import net.minecraft.block.ShulkerBoxBlock;
import net.minecraft.block.StandingSignBlock;
import net.minecraft.block.StructureBlock;
import net.minecraft.block.WallBannerBlock;
import net.minecraft.block.WallSignBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.chunk.ChunkRender;
import net.minecraft.client.renderer.tileentity.BannerTileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.renderer.vertex.VertexFormatElement;
import net.minecraft.item.DyeColor;
import net.minecraft.tileentity.BannerPattern;
import net.minecraft.tileentity.BannerTileEntity;
import net.minecraft.tileentity.BedTileEntity;
import net.minecraft.tileentity.BellTileEntity;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.ConduitTileEntity;
import net.minecraft.tileentity.EndGatewayTileEntity;
import net.minecraft.tileentity.EndPortalTileEntity;
import net.minecraft.tileentity.EnderChestTileEntity;
import net.minecraft.tileentity.ShulkerBoxTileEntity;
import net.minecraft.tileentity.SignTileEntity;
import net.minecraft.tileentity.SkullTileEntity;
import net.minecraft.tileentity.StructureBlockTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;

public class RenderSubChunk {


	// chunk cords
	private int x, y, z;
	private int drawingBlocks;
	private int totalBlocks;
	private boolean bufferDirty;

	private boolean building;

	VertexBuffer buffer;

	public ArrayList<TileEntity> tileEntities;

	public RenderSubChunk(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		bufferDirty = true;
		tileEntities = new ArrayList<>();
	}

	/**
	 * Renders the subcunk if buffer is flaged for redraw, redraws the buffer before
	 * rendering
	 * 
	 * @param allowBufferReload
	 * @return
	 */
	public void render() {

		checkForBuilding();

		if (buffer != null) {
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			buffer.bindBuffer();
			this.setupArrayPointers();
			buffer.drawArrays(7);
			VertexBuffer.unbindBuffer();
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
			GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}

	}

	public void renderTileEntity() {
		for (TileEntity e : tileEntities) {
			BlockTint t = checkAndGetTint(e.getPos(), e.getBlockState());
			if (t == BlockTint.ALLREADY_ALL_RIGHT) {
				continue;
			}
			GlStateManager.color4f(((float) t.getR()) / 255f, ((float) t.getG()) / 255f, ((float) t.getB()) / 255f,
					200f / 255f);
			TileEntityRendererDispatcher.instance.getRenderer(e).render(e, e.getPos().getX(), e.getPos().getY(),
					e.getPos().getZ(), 1, -1);
			if (e instanceof EndPortalTileEntity) {
				GlStateManager.disableLighting();
			}
		}
		GlStateManager.disableFog();
	}

	public void remove() {
		if (buffer != null)
			buffer.deleteGlBuffers();
		buffer = null;

	}

	/**
	 * Checks the buffer for rebuilding
	 */
	private void checkForBuilding() {
		if (bufferDirty) {
			reloadBuffer();
		}

	}

	/**
	 * Sets up Opengl stuff
	 */
	private void setupArrayPointers() {
		GlStateManager.vertexPointer(3, 5126, 28, 0);
		GlStateManager.colorPointer(4, 5121, 28, 12);
		GlStateManager.texCoordPointer(2, 5126, 28, 16);
		GLX.glClientActiveTexture(GLX.GL_TEXTURE1);
		GlStateManager.texCoordPointer(2, 5122, 28, 24);
		GLX.glClientActiveTexture(GLX.GL_TEXTURE0);
	}

	/**
	 * Renders the debug box
	 */
	public void renderBox() {
		BoxRenderer.renderBounds(getFirstWorldXCoord(), getFirstWorldYCoord(), getFirstWorldZCoord(),
				getFirstWorldXCoord() + 16, getFirstWorldYCoord() + 16, getFirstWorldZCoord() + 16);
	}

	/**
	 * Gets the total ammount of blocks
	 * 
	 * @return
	 */
	public int getTotalBlocks() {
		return totalBlocks;
	}

	/**
	 * Gets the ammount of drawn blocks
	 * 
	 * @return
	 */
	public int getDrawingBlocks() {
		return drawingBlocks;
	}

	/**
	 * Performs buffer reload must be called from OpenGL Thread or a thread that had
	 * the gl context handed over
	 */
	public void reloadBuffer() {

		if (building) { // dont build if allready building
			return;
		}
		remove();
		building = true;
		bufferDirty = false;
		drawingBlocks = 0;
		totalBlocks = 0;
		tileEntities.clear();
		//workarround for buffer overflow this is actualy shit because of ram issues
		BufferBuilder chunkBuffer = new BufferBuilder(0);
		chunkBuffer.begin(7, DefaultVertexFormats.BLOCK);
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				for (int z = 0; z < 16; z++) {
					int realx = getFirstWorldXCoord() + x;
					int realy = getFirstWorldYCoord() + y;
					int realz = getFirstWorldZCoord() + z;
					RenderabelBlock block = RenderabelSchematic.INSTANCE.getBlock(new BlockPos(realx, realy, realz));
					if (block != null) {
						if(RenderabelSchematic.INSTANCE.getFilter() != null) {
							SchematicFilter filter = RenderabelSchematic.INSTANCE.getFilter();
							if(!filter.shoodRenderBlock(block.getRelativePos(), block.getAbsolutePos())) {
								continue;
							}
						}
						BlockTint t = checkAndGetTint(block.getAbsolutePos(), block.getBlockStateToRender());
						if (t != BlockTint.ALLREADY_ALL_RIGHT) {
							BlockState render = block.getBlockStateToRender();
							if (checkBlockStateForTileEntityRender(render, block.getAbsolutePos())) {
								continue;
							}
							BlockRenderer.INSTANCE.writeBlock(block.getBlockStateToRender(), block.getAbsolutePos(),
									chunkBuffer, t);
							drawingBlocks++;
						}
						totalBlocks++;
					}
				}
			}
		}
		building = false;
		chunkBuffer.sortVertexData((float)TileEntityRendererDispatcher.staticPlayerX, (float)TileEntityRendererDispatcher.staticPlayerY, (float)TileEntityRendererDispatcher.staticPlayerZ);
		if (buffer == null)
			buffer = new VertexBuffer(chunkBuffer.getVertexFormat());
		
		buffer.bufferData(chunkBuffer.getByteBuffer());
		chunkBuffer.finishDrawing();
	}

	private boolean checkBlockStateForTileEntityRender(BlockState state, BlockPos pos) {
		if (state.getBlock() == Blocks.CHEST) {
			ChestTileEntity chestTileEntity = new ChestTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			chestTileEntity.setPos(pos);
			tileEntities.add(chestTileEntity);
			return true;
		}
		if (state.getBlock() == Blocks.ENDER_CHEST) {
			EnderChestTileEntity chestTileEntity = new EnderChestTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			chestTileEntity.setPos(pos);
			tileEntities.add(chestTileEntity);
			return true;
		}
		if (state.getBlock() instanceof WallSignBlock || state.getBlock() instanceof StandingSignBlock) {
			SignTileEntity entity = new SignTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof BedBlock) {
			BedTileEntity entity = new BedTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof ShulkerBoxBlock) {
			ShulkerBoxTileEntity entity = new ShulkerBoxTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof BannerBlock || state.getBlock() instanceof WallBannerBlock) {
			// banner is a bit more work...
			BannerTileEntity entity = new BannerTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}

				@Override
				public String getPatternResourceLocation() {
					return "d" + getColorList().get(0).getId();
				}

				private List<BannerPattern> patterns;

				@Override
				public List<BannerPattern> getPatternList() {
					if (patterns == null) {
						patterns = new ArrayList<>();
						patterns.add(BannerPattern.BASE);
					}
					return patterns;
				}

				private List<DyeColor> colors;

				@Override
				public List<DyeColor> getColorList() {
					if (colors == null) {
						colors = new ArrayList<>();
						colors.add(((AbstractBannerBlock) this.getBlockState().getBlock()).getColor());
					}
					return colors;
				}
			};

			entity.setWorld(Minecraft.getInstance().world);
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof AbstractSkullBlock) {
			SkullTileEntity entity = new SkullTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof BellBlock) {
			BellTileEntity entity = new BellTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return false; // still need the blockrenderer ._.
		}
		if (state.getBlock() instanceof ConduitBlock) {
			ConduitTileEntity entity = new ConduitTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return false; // still need the blockrenderer ._. but is a bit buggy
		}
		if (state.getBlock() instanceof EndGatewayBlock) {
			EndGatewayTileEntity entity = new EndGatewayTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setWorld(Minecraft.getInstance().world);
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		if (state.getBlock() instanceof EndPortalBlock) {
			EndPortalTileEntity entity = new EndPortalTileEntity() {
				@Override
				public BlockState getBlockState() {
					return state;
				}
			};
			entity.setPos(pos);
			tileEntities.add(entity);
			return true;
		}
		return false;
	}

	/**
	 * Returns needed blocktint for given blockpos
	 * 
	 * @param to
	 * @param intendet
	 * @return
	 */
	public static BlockTint checkAndGetTint(BlockPos to, BlockState intendet) {
		BlockState inworld = Minecraft.getInstance().world.getBlockState(to);
		if (intendet.isAir() && !inworld.isAir()) {
			return BlockTint.REQUIRE_AIR;
		}
		if (inworld.isAir() && !intendet.isAir()) {
			return BlockTint.MISSING_BLOCK;
		}
		if (inworld.equals(intendet)) {
			return BlockTint.ALLREADY_ALL_RIGHT;
		}
		if (inworld.getBlock().equals(intendet.getBlock())) {
			return BlockTint.WRONG_STATE;
		} else {
			return BlockTint.WRONG_BLOCK;
		}
	}

	/**
	 * Flags the subchunk dirty
	 */
	public void flagDirty() {
		bufferDirty = true;
	}

	/**
	 * True if chunk dosent render anything
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return drawingBlocks == 0 && !bufferDirty;
	}

	/**
	 * First realworld x coord
	 * 
	 * @return
	 */
	public int getFirstWorldXCoord() {
		return x << 4;
	}

	/**
	 * First realworld y coord
	 * 
	 * @return
	 */
	public int getFirstWorldYCoord() {
		return y << 4;
	}

	/**
	 * First realworld z coord
	 * 
	 * @return
	 */
	public int getFirstWorldZCoord() {
		return z << 4;
	}

	@Override
	protected void finalize() throws Throwable {
		if (buffer != null) {
			throw new ResourceLeakException("Subchunk at " + x + " " + y + " " + z + " has not released the gpu Buffer");
		}
		super.finalize();
	}

}
