package de.starmixcraft.libmatica.renderer.chunk;

public class RenderChunk {

	private RenderSubChunk[] subChunks;

	private int x, z; // chunk cords

	private int subChunkCount;

	boolean dirty;
	boolean drawing;

	public RenderChunk(int x, int z) {
		this.x = x;
		this.z = z;
		subChunks = new RenderSubChunk[16];
		flagDirty();
	}

	/**
	 * renders the chunk to the world
	 * 
	 * @param allowBufferReload
	 * @return
	 */
	public void render() {
		int subChunkCount = 0;
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			chunk.render();

			if (chunk.isEmpty()) {
				subChunks[i].remove();
				subChunks[i] = null;
			}
			if (subChunks[i] != null) {
				subChunkCount++;
			}
		}
		this.subChunkCount = subChunkCount;
	}

	/**
	 * gets the total amount of blocks in this chunk
	 * 
	 * @return
	 */
	public int getTotalBlocks() {
		int j = 0;
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			j += chunk.getTotalBlocks();
		}
		return j;
	}

	/**
	 * gets the amount of actual renders blocks in this chunk
	 * 
	 * @return
	 */
	public int getDrawingBlocks() {
		int j = 0;
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			j += chunk.getDrawingBlocks();
		}
		return j;
	}
	

	public int getTileEntitys() {
		int j = 0;
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			j += chunk.tileEntities.size();
		}
		return j;
	}


	
	public void renderTileEntitys() {
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			chunk.renderTileEntity();
		}
	}
	
	/**
	 * Renders the debug boxes
	 */
	public void renderBox() {
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			chunk.renderBox();
		}
	}

	/**
	 * Gets the subchunk at y
	 * 
	 * @param y
	 * @return
	 */
	public RenderSubChunk getSubChunk(int y) {
		RenderSubChunk chunk = subChunks[y];
		if (chunk == null) {
			subChunks[y] = new RenderSubChunk(x, y, z);
		}
		return subChunks[y];
	}

	/**
	 * Flag chunk dirty for buffer recash
	 */
	public void flagDirty() {
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			chunk.flagDirty();
		}
	}

	public void clear() {
		for (int i = 0; i < subChunks.length; i++) {
			RenderSubChunk chunk = subChunks[i];
			if (chunk == null) {
				continue;
			}
			chunk.remove();
			subChunks[i] = null;
		}
	}

	/**
	 * Gets the chunk x
	 * 
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the chunk z
	 * 
	 * @return
	 */
	public int getZ() {
		return z;
	}

	/**
	 * true if the chunk renders 0 blocks
	 * 
	 * @return {@link Boolean}
	 */
	public boolean isEmpty() {
		return subChunkCount == 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RenderChunk)) {
			return false;
		}
		RenderChunk other = (RenderChunk) obj;
		return other.x == x && other.z == z;
	}
}
