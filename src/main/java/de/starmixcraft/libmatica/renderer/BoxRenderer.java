package de.starmixcraft.libmatica.renderer;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class BoxRenderer {
	/**
	 * Renders a box arroung the cords
	 * @param xmin
	 * @param ymin
	 * @param zmin
	 * @param xmax
	 * @param ymax
	 * @param zmax
	 */
	public static void renderBounds(int xmin, int ymin, int zmin, int xmax, int ymax, int zmax) {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuffer();
		GlStateManager.disableLighting();
		GlStateManager.disableTexture();
		GlStateManager.enableBlend();
		GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
				GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
				GlStateManager.DestFactor.ZERO);
		renderBox(tessellator, vertexbuffer, xmin, ymin, zmin, xmax, ymax, zmax, 255, 200, 1);
        GlStateManager.lineWidth(1.0F);
        GlStateManager.enableLighting();
        GlStateManager.enableTexture();
        GlStateManager.enableDepthTest();
        GlStateManager.depthMask(true);
	}

	private static void renderBox(Tessellator tesselator, BufferBuilder bbuffer, double xMin, double yMin, double zMin,
			double xMax, double yMax, double zMax, int alpha, int p_190055_16_, int p_190055_17_) {
		GlStateManager.lineWidth(2.0F);
		bbuffer.begin(3, DefaultVertexFormats.POSITION_COLOR);
		BlockRenderer.INSTANCE.translate(bbuffer);
		bbuffer.pos(xMin, yMin, zMin).color((float) p_190055_16_, (float) p_190055_16_, (float) p_190055_16_, 0.0F)
				.endVertex();
		bbuffer.pos(xMin, yMin, zMin).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMin, zMin).color(p_190055_16_, p_190055_17_, p_190055_17_, alpha).endVertex();
		bbuffer.pos(xMax, yMin, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMin, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMin, zMin).color(p_190055_17_, p_190055_17_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMax, zMin).color(p_190055_17_, p_190055_16_, p_190055_17_, alpha).endVertex();
		bbuffer.pos(xMax, yMax, zMin).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMax, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMax, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMax, zMin).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMax, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMin, yMin, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMin, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMax, zMax).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMax, zMin).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMin, zMin).color(p_190055_16_, p_190055_16_, p_190055_16_, alpha).endVertex();
		bbuffer.pos(xMax, yMin, zMin).color((float) p_190055_16_, (float) p_190055_16_, (float) p_190055_16_, 0.0F)
				.endVertex();
		
		tesselator.draw();
		tesselator.getBuffer().setTranslation(0, 0, 0);
		GlStateManager.lineWidth(1.0F);
	}
}
