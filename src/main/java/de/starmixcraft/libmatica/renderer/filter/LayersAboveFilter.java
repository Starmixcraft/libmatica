package de.starmixcraft.libmatica.renderer.filter;

import net.minecraft.util.math.BlockPos;

public class LayersAboveFilter extends LayeredFilter {

	public LayersAboveFilter(int layer) {
		super(layer);
	}

	@Override
	public boolean shoodRenderBlock(BlockPos relative, BlockPos absolute) {
		// TODO Auto-generated method stub
		return relative.getY() > layer;
	}
}
