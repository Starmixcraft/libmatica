package de.starmixcraft.libmatica.renderer.filter;

import net.minecraft.util.math.BlockPos;

public class LayersBelowFilter extends LayeredFilter {

	
	
	public LayersBelowFilter(int layer) {
		super(layer);
	}

	@Override
	public boolean shoodRenderBlock(BlockPos relative, BlockPos absolute) {
		return relative.getY() < layer;
	}

}
