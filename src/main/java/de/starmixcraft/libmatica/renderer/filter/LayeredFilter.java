package de.starmixcraft.libmatica.renderer.filter;

import net.minecraft.util.math.BlockPos;

public class LayeredFilter extends SchematicFilter{

	
	
	public LayeredFilter(int layer) {
		this.layer = layer;
	}
	
	public void setLayer(int layer) {
		this.layer = layer;
	}
	
	protected int layer;

	@Override
	public boolean shoodRenderBlock(BlockPos relative, BlockPos absolute) {
		return relative.getY() == layer;
	}
}
