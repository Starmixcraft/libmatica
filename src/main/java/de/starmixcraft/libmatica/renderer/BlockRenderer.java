package de.starmixcraft.libmatica.renderer;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.ButtonGroup;

import org.lwjgl.opengl.GL11;
import org.omg.PortableServer.POA;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import de.starmixcraft.libmatica.schematic.RenderabelBlock;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.block.AirBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.renderer.vertex.VertexFormatElement;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.state.IProperty;
import net.minecraft.tileentity.SignTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IEnviromentBlockReader;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockRenderer {

	private static final Random rad = new Random();
	public static BlockRenderer INSTANCE;

	public BlockRenderer() {
		INSTANCE = this;
	}

	double backTelletationX;
	double backTelletationY;
	double backTelletationZ;

	boolean rendering;

	private int renderdBlockStates;
	private long start;
	private long end;

	/**
	 * Starts the Blockrenderes and enables rendering of things
	 */
	public void begin() {
		if (rendering) {
			return;
		}

		rendering = true;
		renderdBlockStates = 0;
		start = System.nanoTime();

		
		GlStateManager.pushMatrix();
		GlStateManager.disableLighting();
		GlStateManager.enableBlend();
		GlStateManager.enableCull();
		GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
//		GlStateManager.blendFuncSeparate(SourceFactor.SRC_ALPHA,
//				DestFactor.ONE_MINUS_SRC_ALPHA,SourceFactor.ONE,
//				DestFactor.ZERO);


		Minecraft.getInstance().getTextureManager().bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
		GlStateManager.enableDepthTest();
		GlStateManager.translated(-backTelletationX, -backTelletationY, -backTelletationZ);

	}
	
	
	public void initTranslator() {
		backTelletationX = TileEntityRendererDispatcher.staticPlayerX;
		backTelletationY = TileEntityRendererDispatcher.staticPlayerY;
		backTelletationZ = TileEntityRendererDispatcher.staticPlayerZ;
	}

	/**
	 * Renders the given buffer to the screen
	 * 
	 * @param buffer
	 */
	public void renderBuffer(BufferBuilder buffer) {
		if (!rendering) {
			throw new Error("Tryed to render a block whilst renderer not started");
		}
		if (buffer.getVertexCount() > 0) {
			VertexFormat vertexformat = buffer.getVertexFormat();
			int i = vertexformat.getSize();
			ByteBuffer bytebuffer = buffer.getByteBuffer();
			List<VertexFormatElement> list = vertexformat.getElements();

			for (int j = 0; j < list.size(); ++j) {
				VertexFormatElement vertexformatelement = list.get(j);
				vertexformatelement.getUsage().preDraw(vertexformat, j, i, bytebuffer); // moved to
																						// VertexFormatElement.preDraw
			}
			GlStateManager.drawArrays(buffer.getDrawMode(), 0, buffer.getVertexCount());
			int i1 = 0;

			for (int j1 = list.size(); i1 < j1; ++i1) {
				VertexFormatElement vertexformatelement1 = list.get(i1);
				vertexformatelement1.getUsage().postDraw(vertexformat, i1, i, bytebuffer); // moved to
																							// VertexFormatElement.postDraw
			}
		}
	}

	private static BlockState airBlockState = new BlockState(Blocks.AIR, ImmutableMap.of()) {
		public net.minecraft.block.BlockRenderType getRenderType() {
			return BlockRenderType.MODEL;
		};

		{
		};
	};

	private static final BufferBuilder writeBlockBuffer = new BufferBuilder(16);

	/**
	 * Writes render data for a block to the bufferbuilder
	 * 
	 * @param render
	 * @param to
	 * @param reader
	 * @param bufferBuilder
	 */
	public void writeBlock(BlockState render, BlockPos to, BufferBuilder bufferBuilder, BlockTint tint) {
//		if(!rendering) {
//			throw new Error("Tryed to write a block whilst renderer not started");
//		}
		if (render == null || to == null) {
			return;
		}
		writeBlockBuffer.begin(7, DefaultVertexFormats.BLOCK);

//		if(render.getBlock() == Blocks.AIR) {
//			render = airBlockState;
//		}

		BlockRendererDispatcher blockrendererdispatcher = Minecraft.getInstance().getBlockRendererDispatcher();
		if (!render.getFluidState().isEmpty())
			blockrendererdispatcher.renderFluid(to, RenderabelSchematic.INSTANCE, writeBlockBuffer, render.getFluidState());

		blockrendererdispatcher.renderBlock(render, to, RenderabelSchematic.INSTANCE, writeBlockBuffer, rad,
				EmptyModelData.INSTANCE);
		if(tint == null) {
			tint = BlockTint.ALLREADY_ALL_RIGHT;
		}
		int r = tint.getR();
		int g = tint.getG();
		int b = tint.getB();
		int a = 200;
		float blend = tint.getTint();

		IntBuffer buffer = writeBlockBuffer.getByteBuffer().asIntBuffer();
		for (int i = 0; i < writeBlockBuffer.getVertexCount(); i++) {
			int colorIndex = writeBlockBuffer.getColorIndex(i);
			int color = buffer.get(colorIndex);
			int r2 = 0;
			int g2 = 0;
			int b2 = 0;
			if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
				r2 = (int) ((float) (color & 255));
				g2 = (int) ((float) (color >> 8 & 255));
				b2 = (int) ((float) (color >> 16 & 255));
			} else {
				r2 = (int) ((float) (color >> 24 & 255) );
				g2 = (int) ((float) (color >> 16 & 255) );
				b2 = (int) ((float) (color >> 8 & 255) );
			}
			Color tinit = new Color(r, g, b, a);
			Color base = new Color(r2, g2, b2);
			Color finalColor = blend(base, tinit, blend);
			writeBlockBuffer.putColorRGBA(colorIndex, finalColor.getRed(), finalColor.getGreen(), finalColor.getBlue(), a);
		}
		
		bufferBuilder.addVertexData(writeBlockBuffer.getVertexState().getRawBuffer());
		writeBlockBuffer.finishDrawing();
		renderdBlockStates++;
	}
	
	Color blend( Color c1, Color c2, float ratio ) {
	    if ( ratio > 1f ) ratio = 1f;
	    else if ( ratio < 0f ) ratio = 0f;
	    float iRatio = 1.0f - ratio;

	    int i1 = c1.getRGB();
	    int i2 = c2.getRGB();

	    int a1 = (i1 >> 24 & 0xff);
	    int r1 = ((i1 & 0xff0000) >> 16);
	    int g1 = ((i1 & 0xff00) >> 8);
	    int b1 = (i1 & 0xff);

	    int a2 = (i2 >> 24 & 0xff);
	    int r2 = ((i2 & 0xff0000) >> 16);
	    int g2 = ((i2 & 0xff00) >> 8);
	    int b2 = (i2 & 0xff);

	    int a = (int)((a1 * iRatio) + (a2 * ratio));
	    int r = (int)((r1 * iRatio) + (r2 * ratio));
	    int g = (int)((g1 * iRatio) + (g2 * ratio));
	    int b = (int)((b1 * iRatio) + (b2 * ratio));

	    return new Color( a << 24 | r << 16 | g << 8 | b );
	}

	/**
	 * Draws 1 block to the world, is verry inefficent
	 * 
	 * @param toRender
	 * @param to
	 */
	public void drawBlock(BlockState toRender, BlockPos to) {
		if (toRender == null || to == null) {
			return;
		}
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.BLOCK);
		translate(vertexbuffer); // translation is emportant
		writeBlock(toRender, to, vertexbuffer, null);
		tessellator.draw();
		renderdBlockStates++;
	}

	/**
	 * translates back to 0,0,0 to draw with world pos
	 * 
	 * @param vertexbuffer
	 */
	public void translate(BufferBuilder vertexbuffer) {
		vertexbuffer.setTranslation(-backTelletationX, -backTelletationY, -backTelletationZ);
	}

	/**
	 * Gets the debug info
	 * 
	 * @return
	 */
	public String getDebugInfo() {
		return "SchemRenderer: RB: " + renderdBlockStates + ", MSR: " + getExactMSR();
	}

	/**
	 * Gets the ms that the rendering took
	 * 
	 * @return
	 */
	public String getExactMSR() {
		double nanosTook = (end - start);
		double dif = 1000 * 1000;
		return (nanosTook > 10 * dif ? "�c" : "�a") + String.format("%.3f", nanosTook / dif);
	}

	/**
	 * ends the rendering and resets opengl things
	 */
	public void end() {
		if (!rendering) {
			return;
		}
		Tessellator.getInstance().getBuffer().setTranslation(0, 0, 0);
		GlStateManager.enableLighting();
		GlStateManager.disableBlend();
		GlStateManager.disableCull();
		GlStateManager.popMatrix();
		
		rendering = false;
		end = System.nanoTime();
	}

}
