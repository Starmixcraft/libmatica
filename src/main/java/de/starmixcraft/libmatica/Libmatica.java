package de.starmixcraft.libmatica;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.starmixcraft.libmatica.handler.WorldHandler;
import de.starmixcraft.libmatica.renderer.BlockRenderer;
import de.starmixcraft.libmatica.renderer.chunk.RenderSubChunk;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import de.starmixcraft.libmatica.schematic.io.SchemFileManager;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.state.IProperty;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("libmatica")
public class Libmatica {
	// Directly reference a log4j logger.
	private static final Logger LOGGER = LogManager.getLogger();

	public Libmatica() {
		// Register the setup method for modloading
		// Register the doClientStuff method for modloading
		MinecraftForge.EVENT_BUS.register(new WorldHandler());

		// Register ourselves for server and other game events we are interested in
		MinecraftForge.EVENT_BUS.register(this);
		schemFileManager = new SchemFileManager();
	}

	public static BlockRenderer r = new BlockRenderer();

	private static final String DEBUG_PREFIX = "�6[Libmatica] �r";
	
	public static SchemFileManager schemFileManager;

	@SubscribeEvent
	public void onOverlyEvent(RenderGameOverlayEvent.Text e) {
		if (Minecraft.getInstance().gameSettings.showDebugInfo) {
			e.getLeft().add("");
			e.getLeft().add(DEBUG_PREFIX + r.getDebugInfo());
			if (RenderabelSchematic.INSTANCE != null) {
				e.getLeft().add(DEBUG_PREFIX + RenderabelSchematic.INSTANCE.getRenderer().getDebugInfo());
				e.getLeft().add(DEBUG_PREFIX + RenderabelSchematic.INSTANCE.getRenderer().getDebugInfoTileEntitys());
				BlockRayTraceResult result = RenderabelSchematic.INSTANCE.mouseOver;
				if (result.getType() == Type.BLOCK) {
					BlockState hit = RenderabelSchematic.INSTANCE.getBlockState(result.getPos());
					e.getRight().add("");
					e.getRight().add(DEBUG_PREFIX + "TargetBlock: " + result.getPos().getX() + ", "
							+ result.getPos().getY() + ", " + result.getPos().getZ());
					e.getRight().add("�6Type: " + hit.getBlock().getNameTextComponent().getFormattedText());
					e.getRight().add("�6Blocktint: " + RenderSubChunk.checkAndGetTint(result.getPos(),
							RenderabelSchematic.INSTANCE.getBlockState(result.getPos())));
					e.getRight().add("�6Properties:");
					for (IProperty<?> a : hit.getProperties()) {
						e.getRight().add("�6" + a.getName() + ": " + hit.get(a).toString());
					}
				} else if (result.getType() == Type.MISS) {
					e.getRight().add("");
					e.getRight().add(DEBUG_PREFIX + "TargetBlock: None");

				}
			}

		}
	}

}
