package de.starmixcraft.libmatica.handler;

import org.lwjgl.glfw.GLFW;

import de.starmixcraft.libmatica.Libmatica;
import de.starmixcraft.libmatica.gui.LibmaticaMainGui;
import de.starmixcraft.libmatica.gui.LoadSchematicGui;
import de.starmixcraft.libmatica.gui.SaveSchematicGui;
import de.starmixcraft.libmatica.renderer.BoxRenderer;
import de.starmixcraft.libmatica.schematic.RenderabelSchematic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.screen.IngameMenuScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.DropperTileEntity;
import net.minecraft.tileentity.SignTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.client.event.GuiScreenEvent.InitGuiEvent;
import net.minecraftforge.client.event.InputEvent.KeyInputEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;

public class WorldHandler {

	@SubscribeEvent
	public void onChat(ClientChatEvent e) {
		if (e.getMessage().equalsIgnoreCase("schem")) {
			RenderabelSchematic schem = new RenderabelSchematic(new BlockPos(150, 110, 250), 1, 200, 100);
			schem.setInstance();
		}
		if (e.getMessage().equalsIgnoreCase("r")) {
			RenderabelSchematic.INSTANCE.rotate(Rotation.CLOCKWISE_90);
		}
		if (e.getMessage().equalsIgnoreCase("f")) {
			RenderabelSchematic.INSTANCE.flagAllChunksInSchemDirty(false);
		}
//		schem.getRenderer().flagPosDirty(new BlockPos(100, 60, 100));
//		schem.flagAllChunksInSchemDirty();
	}
	
	
	@SubscribeEvent
	public void onB(KeyInputEvent e) {
		if(Minecraft.getInstance().currentScreen == null) {
			if(e.getKey() == GLFW.GLFW_KEY_F7) {
				Minecraft.getInstance().displayGuiScreen(new LoadSchematicGui.GuiScreen());
			}else
			if(e.getKey() == GLFW.GLFW_KEY_F8) {
				Minecraft.getInstance().displayGuiScreen(new SaveSchematicGui());
			}
		}
	}

	@SubscribeEvent
	public void onA(BlockEvent e) {
		if (RenderabelSchematic.INSTANCE != null) {
			for (Direction d : Direction.values()) {
				BlockPos offset = e.getPos().offset(d);
				if (RenderabelSchematic.INSTANCE.isInSchem(offset)) {
					RenderabelSchematic.INSTANCE.getRenderer().flagPosDirty(offset);
				}
			}
			if (RenderabelSchematic.INSTANCE.isInSchem(e.getPos())) {
				RenderabelSchematic.INSTANCE.getRenderer().flagPosDirty(e.getPos());

			}
		}
	}

	@SubscribeEvent
	public void onGuiOpen(InitGuiEvent e) {
		if (e.getGui() instanceof IngameMenuScreen) {
			Button b = new Button(e.getGui().width / 2 - 102, e.getGui().height / 4 + 144 + -16, 204, 20, "Libmatica",
					(button) -> {
						Minecraft.getInstance().displayGuiScreen(new LibmaticaMainGui());
					});
			e.addWidget(b);
		}
	}

	@SubscribeEvent
	public void onWorldRender(RenderWorldLastEvent event) {
		Libmatica.r.initTranslator();
		if (RenderabelSchematic.INSTANCE != null) {
			Libmatica.r.begin();
			RenderabelSchematic.INSTANCE.render();
			RenderabelSchematic.INSTANCE.getRenderer().renderTileEntitys();
			Libmatica.r.end();
			RenderabelSchematic.INSTANCE.renderBox();
		}
		if (SaveSchematicGui.showBounds) {
			BlockPos pos1 = SaveSchematicGui.pos1;
			BlockPos pos2 = SaveSchematicGui.pos2;
			BoxRenderer.renderBounds(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()),
					Math.min(pos1.getZ(), pos2.getZ()), Math.max(pos1.getX(), pos2.getX()),
					Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));
		}
	}
}
